jQuery(document).ready(function() {

	jQuery('#updateLimeAjax').click(limeLiveCategoryAjax)

	window.setInterval(limeLiveCategoryAjax, 5 * 60 * 1000); // 5 minutes

	function limeLiveCategoryAjax()
	{
		jQuery.post(
			// see tip #1 for how we declare global javascript variables
			LimeAjax.ajaxurl,
			{
				// here we declare the parameters to send along with the request
				// this means the following action hooks will be fired:
				// wp_ajax_nopriv_ghost-live-category and wp_ajax_ghost-live-category
				action : 'lime-live-category',

				// other parameters can be added along with "action"
				catID : LimeAjax.catID
			},
			function( response ) {
				jQuery('.lime_live_category_output').html(response);
			}
		);
	}

});