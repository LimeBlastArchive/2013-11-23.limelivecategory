<?php

/**
 * Plugin Name: limeLiveCategory
 * Plugin URI: http://ghostds.com/
 * Description: A widget that displays the latest posts from a specified category, and uses ajax to update it every 5 minutes.
 * Version: 0.1
 * Author: Daniel Hollands
 * Author URI: http://limeblast.co.uk/
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
/**
 * Add function to widgets_init that'll load our widget.
 * @since 0.1
 */
add_action('widgets_init', 'lime_live_category_load_widgets');

/**
 * Register our widget.
 */
function lime_live_category_load_widgets()
{
	register_widget('Lime_Live_Category_Widget');
}

/**
 * limeLiveCategory  Widget class.
 */
class Lime_Live_Category_Widget extends WP_Widget {

	/**
	 * constructor
	 */
	function Lime_Live_Category_Widget()
	{
		/* Widget settings. */
		$widget_ops = array('classname' => 'lime-live-category', 'description' => __('A widget that displays the latest posts from a specified category, and uses ajax to update it every 5 minutes.', 'ghost-live-category'));

		/* Widget control settings. */
		$control_ops = array('width' => 300, 'height' => 350, 'id_base' => 'lime-live-category');

		/* Create the widget. */
		$this->WP_Widget('lime-live-category', __('Lime Live Category', 'lime-live-category'), $widget_ops, $control_ops);

		/* http://www.garyc40.com/2010/03/5-tips-for-using-ajax-in-wordpress/#js-global */
		// if both logged in and not logged in users can send this AJAX request,
		// add both of these actions, otherwise add only the appropriate one
		add_action('wp_ajax_nopriv_lime-live-category', array(&$this, 'ajax'));
		add_action('wp_ajax_lime-live-category', array(&$this, 'ajax'));
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget($args, $instance)
	{
		extract($args);

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title']);
		$cat = $instance['cat'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title (before and after defined by themes). */
		echo $before_title . $title . $after_title;

		$this->output($cat);

		echo '<a href="#" id="updateLimeAjax">Update</a>';

		/* After widget (defined by themes). */
		echo $after_widget;

		/* http://www.garyc40.com/2010/03/5-tips-for-using-ajax-in-wordpress/#js-global */
		// embed the javascript file that makes the AJAX request
		wp_enqueue_script('lime-live-category-ajax-request', plugin_dir_url(__FILE__) . 'ajax.js', array('jquery'));
		// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
		wp_localize_script('lime-live-category-ajax-request', 'LimeAjax', array('ajaxurl' => admin_url('admin-ajax.php'), 'catID' => $cat));
		// pull in the CSS required for the widget
		wp_enqueue_style('lime-live-category-css', plugin_dir_url(__FILE__) . 'style.css');
	}

	/**
	 * Update the widget settings.
	 */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags($new_instance['title']);

		$instance['cat'] = $new_instance['cat'];

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form($instance)
	{

		/* Set up some default widget settings. */
		$defaults = array('title' => __('Latest Posts', 'lime-live-category'));
		$instance = wp_parse_args((array) $instance, $defaults);
		?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'hybrid'); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category:', 'lime-live-category'); ?></label>
			<?php wp_dropdown_categories(array('name' => $this->get_field_name("cat"), 'selected' => $instance["cat"], 'hierarchical' => 1)); ?>
		</p>

		<?php
	}

	/**
	 * method called when ajax request from the widget is made
	 */
	function ajax()
	{
		$this->output($_POST['catID']);
		// IMPORTANT: don't forget to "exit"
		exit;
	}

	// customise the length of the except
	function new_excerpt_length($length)
	{
		return 10;
	}

	function new_excerpt_more($more)
	{
		return '';
	}

	/**
	 * Outputs the changable html
	 */
	function output($cat)
	{

		// customise the length of the except
		add_filter('excerpt_length', array(&$this, 'new_excerpt_length'));
		add_filter('excerpt_more', array(&$this, 'new_excerpt_more'));

		echo '<div class="lime_live_category_output">';

		// The Query
		$args = array(
			'cat' => $cat,
			'posts_per_page' => 5
		);
		$the_query = new WP_Query($args);

		echo '<ul>';

		// The Loop
		while ($the_query->have_posts()) : $the_query->the_post();
		?>
			<li>
				<div>
					<h3>
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h3>
					<p><?php the_excerpt(); ?></p>
					<a href="<?php the_permalink(); ?>">[+] read more</a>
				</div>
			</li>
		<?php
		endwhile;

		echo '</ul>';
		echo '</div>';

		// Reset Post Data
		wp_reset_postdata();
	}

}